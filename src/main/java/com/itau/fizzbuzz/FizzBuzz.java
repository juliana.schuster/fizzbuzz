package com.itau.fizzbuzz;

import java.util.Arrays;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "fizzbuzz";
		}
		
		if (i % 5 == 0) {
			return "buzz";
		}
		
		if ( i % 3 == 0) {
			return "fizz";
			
		}

		return Integer.toString(i);		
	}

	public static String fizzBuzzRefat(int i) {
		String[] fizzBuzzArray = new String[i];
		int n = 1;
		for (int j=0; j<i; j++) {
			if ((n % 3 ==0) && (n % 5 ==0))  {
				 fizzBuzzArray[j] = "FizzBuzz";
			} else 			
			if (n % 5 == 0) {
				 fizzBuzzArray[j] = "Buzz";
			} else			
			if ( n % 3 == 0) {
				fizzBuzzArray[j] = "Fizz";				
			} else {
			fizzBuzzArray[j] = Integer.toString(n);	
			}
			n++;
		}
		return Arrays.toString(fizzBuzzArray);
	
	}

	
}
